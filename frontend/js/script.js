const login = document.querySelector(".login");
const loginForm = login.querySelector(".login__form");
const loginInput = login.querySelector(".login__input");

const chat = document.querySelector(".chat");
const chatMessages = chat.querySelector(".chat__messages");
const chatForm = chat.querySelector(".chat__form");
const chatInput = chat.querySelector(".chat__input");

const colors = [
  "cadetblue",
  "darkgoldenrod",
  "cornflowerblue",
  "darkkhaki",
  "hotpink",
  "gold",
];

const user = { id: "", name: "", color: "" };

let websocket;

const createMessageSelfElement = (content) => {
  const div = document.createElement("div");
  div.classList.add("message__self");
  div.innerHTML = content;
  return div;
};

const createMessageOtherElement = (content, sender, senderColor) => {
  const div = document.createElement("div");
  const span = document.createElement("span");
  div.classList.add("message__other");
  span.classList.add("message__sender");
  span.style.color = senderColor;
  div.appendChild(span);
  span.innerHTML = sender;
  div.innerHTML += content;
  return div;
};

const getRandomColor = () => {
  const randomIndex = Math.floor(Math.random() * colors.length);
  return colors[randomIndex];
  // return colors[Math.floor(Math.random() * colors.length)];
};

const scrollScreen = () => {
  window.scrollTo({
    top: document.body.scrollHeight,
    behavior: "smooth",
  });
};

const processMessage = ({ data }) => {
  const { userId, userName, userColor, content } = JSON.parse(data);
  const element =
    userId == user.id
      ? createMessageSelfElement(content)
      : createMessageOtherElement(content, userName, userColor);
  chatMessages.appendChild(element);
  scrollScreen();
};

const handleLogin = (event) => {
  event.preventDefault();
  user.id = crypto.randomUUID();
  user.name = loginInput.value;
  user.color = getRandomColor();

  login.style.display = "none";
  chat.style.display = "flex";

  websocket = new WebSocket("wss://chat-backend-28y9.onrender.com");
  websocket.onopen = () =>
    websocket.send(`Usuário: ${user.name} entrou no chat!`);
  websocket.onmessage = processMessage;
  console.log(user);
};

const sendMessage = (event) => {
  event.preventDefault();
  const message = {
    userId: user.id,
    userName: user.name,
    userColor: user.color,
    content: chatInput.value,
  };
  websocket.send(JSON.stringify(message));
  chatInput.value = "";
};

loginForm.addEventListener("submit", handleLogin);
chatForm.addEventListener("submit", sendMessage);

/*
function gerarCorVibrante() {
  var letras = "0123456789ABCDEF";
  var cor = "#";
  var isCorClaraOuEscura = true;

  // Enquanto a cor for clara ou escura, gere uma nova cor
  while (isCorClaraOuEscura) {
    cor = "#";
    for (var i = 0; i < 6; i++) {
      cor += letras[Math.floor(Math.random() * 16)];
    }
    // Verifica se a cor é clara (branca ou próxima), escura (preta) ou cinza
    isCorClaraOuEscura = isCorClaraOuEscuraOuCinza(cor);
  }

  return cor;
}

function isCorClaraOuEscuraOuCinza(cor) {
  // Convertendo a cor hexadecimal para RGB
  var r = parseInt(cor.slice(1, 3), 16);
  var g = parseInt(cor.slice(3, 5), 16);
  var b = parseInt(cor.slice(5, 7), 16);

  // Verificando se a cor está próxima de branco
  var distanciaParaBranco = Math.sqrt(
    Math.pow(r, 2) + Math.pow(g, 2) + Math.pow(b, 2)
  );
  // Verificando se a cor está próxima de preto
  var distanciaParaPreto = Math.sqrt(
    Math.pow(255 - r, 2) + Math.pow(255 - g, 2) + Math.pow(255 - b, 2)
  );
  // Verificando se a cor está próxima de cinza
  var distanciaParaCinza = Math.abs(r - g) + Math.abs(g - b) + Math.abs(b - r);

  return (
    distanciaParaBranco > 500 ||
    distanciaParaPreto < 100 ||
    distanciaParaCinza < 30
  );
}

// Testando a função
console.log(gerarCorVibrante());

*/
